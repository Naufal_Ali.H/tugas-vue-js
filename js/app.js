Vue.component('plus-button', {
    data: function () {
        return {
            count: 0
        }
    },
    template: `
        <div>
            <button v-on:click="count++"> + </button>
            {{ count }} 
        </div>
    `
        
});
Vue.component('minus-button', {
    data: function () {
        return {
            count: 0
        }
    },
    template: '<button v-on:click="count--"> - {{ count }} </button>'
});

var app = new Vue({
    el: '#app',
    data: {
        content:'',
        comments:[],
        buttons:[],
        count: '{{ count }}'
    },
    methods: {
        addComment: function () {
            this.comments.push(this.content);
            this.content = '';
        },
        
    }
});